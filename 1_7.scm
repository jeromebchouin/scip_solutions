;;; SICP Exercise 1.7 code

(define (square x)
	(* x x))

(define (average x y)
	(/ (+ x y) 2))

(define (improve guess x)
	(average guess (/ x guess)))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.000001))

(define (sqrt-iter guess x)
	(if(good-enough? guess x)
		guess
		(sqrt-iter (improve guess x) x)))

(define (sqrt x) (sqrt-iter 1.0 x))



(define (better-good-enough? previous_guess guess)
	(< (abs (- guess previous_guess)) (* guess 0.000001) ))

(define (better-sqrt-iter previous_guess guess x) 
	(if(better-good-enough? previous_guess guess)
		guess
		(better-sqrt-iter guess (improve guess x) x)))


(define (better-sqrt x) (better-sqrt-iter 0.0 1.0 x))