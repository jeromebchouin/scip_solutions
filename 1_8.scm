;;; SICP Exercise 1.8 code

(define (good-enough? previous-guess guess)
	(< (abs (- guess previous-guess)) (* guess 0.000001) ))


(define (improve-guess y x)
	(/ 	(+ 	(* 2 y) 
			(/	x 
				(* y y))) 
		3))

(define (cbrt-iter previous-guess guess x)
	(if (good-enough? previous-guess guess)
		guess
		(cbrt-iter guess (improve-guess guess x) x)))

(define (cbrt x)
	(cbrt-iter 0.0 1.0 x))