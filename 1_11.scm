;;; SICP Exercise 1.11 code

;; Recursive version
(define (recf n)
	(if (< n 3)
		n
		(+ 	(recf (- n 1))
			(* 2 (recf (- n 2))) 
			(* 3 (recf (- n 3))))))


;; Iterative version
(define (itf n)
	(define (itf-iter i val0 val1 val2)
		(if (= i n)
			val0
			(itf-iter (+ i 1) val1 val2 (+ (* 3 val0) (* 2 val1) val2))))

	(itf-iter 0 0 1 2))