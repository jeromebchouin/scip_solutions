# README #

These are my solutions for the exercises in [https://mitpress.mit.edu/sicp/](Structure and Interpretation of Computer Programs 2nd ed).

They are written in Scheme and were tested with guile on Linux.